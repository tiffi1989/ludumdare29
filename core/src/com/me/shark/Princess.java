package com.me.shark;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

public class Princess {
	
	private Body body;
	private BodyDef bodyDef;
	private CircleShape circleShape;
	private FixtureDef fixtureDef;
	private Fixture fixture;
	private int velocity = 20;
	private float radius = 5f;
	private TextureRegion currentTexture;
	private World world;
	private Shark shark;
	private boolean speaking, swimming, dead;
	private Texture currentBalloon;
	float timeDead;
	
	private boolean active;

	
	public Princess(World world, Shark shark) {
		this.world = world;
		this.shark = shark;
		
		bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(160, -10);
		body = world.createBody(bodyDef);
		body.setUserData("princess");
		

		circleShape = new CircleShape();
		circleShape.setRadius(radius);

		fixtureDef = new FixtureDef();
		fixtureDef.shape = circleShape;
		fixtureDef.density = 0.5f;
		fixtureDef.friction = 0.4f;
		fixtureDef.restitution = 0.6f;
		fixtureDef.isSensor = true;

		fixture = body.createFixture(fixtureDef);
		fixture.setUserData("princess");
		

	}
	
	public void come(){
		body.setLinearVelocity(-velocity, velocity);
		Resources.schnell.play();
		Resources.ruhig.stop();
	}
	
	public void go(){
		body.setLinearVelocity(velocity, -velocity);
		Resources.schnell.stop();
		Resources.ruhig.play();
	}
	
	public void update(){
		if(body.getPosition().x < 140 && body.getPosition().y > 10)
			body.setLinearVelocity(0, 0);
		if(body.getPosition().x > 160 && body.getPosition().y < -10)
			body.setLinearVelocity(0, 0);
	}
	
	public TextureRegion getTexture(float deltaAnimation){
		return currentTexture;
	}
	
	public void setActive(boolean bool){
		active = bool;
	}
	
	public String getStage(){
		return "";
	}
	
	public float getX() {
		return body.getPosition().x - radius * 1.25f;
	}

	public float getY() {
		return body.getPosition().y - radius * 1.25f;
	}

	public float getWidth() {
		return radius * 2.5f;
	}

	public float getHeight() {
		return radius * 2.5f;
	}
	
	public void setTexture(TextureRegion texture){
		currentTexture = texture;
	}
	
	public Body getBody(){
		return body;
	}
	
	public void resetPosition(){
		body.setTransform(160, -10, 0);
	}
	
	public boolean isSpeaking(){
		return speaking;
	}
	
	public Texture getBalloon(){
		return currentBalloon;
	}
	
	public void setBalloon(Texture balloon){
		currentBalloon = balloon;
	}
	
	public void setSpeaking(boolean bool){
		speaking = bool;
	}

	public boolean isSwimming() {
		return swimming;
	}

	public void setSwimming(boolean swimming) {
		this.swimming = swimming;
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}
	
	public float timeDead(float delta) {
		if (dead)
			timeDead += delta;
		return timeDead;
	}

	public float getTimeDead() {
		return timeDead;
	}
	
	public float getBloodX() {
		return body.getPosition().x - radius *1.25f  * timeDead;
	}

	public float getBloodY() {
		return body.getPosition().y - radius *1.25f * timeDead;
	}
	


}
