package com.me.shark;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.World;

public class Stages {

	private Princess princess;
	private Shark shark;
	private ArrayList<Child> childs;
	private GameScreen game;
	private World world;

	private int stageDynamite = 10;
	private int stageGun = 20;
	private int stageRl = 30;
	private int stageRlRl = 40;
	private int stageNuc = 50;
	private int stageDynamiteRl = 60;
	private int stageDynamiteGun = 70;
	private int stageGunGun = 80;
	private int stageEnd = 90;

	private float gunStageTime = 15;
	private float dynamiteStageTime = 15;
	private float rlStageTime = 15;
	private float rlRlStageTime = 15;
	private float nucStageTime = 15;
	private float DynamiteRlStageTime = 15;
	private float DynamiteGunStageTime = 15;
	private float gunGunStageTime = 15;
	private float endStageTime = 99999;

	private float speakingTime = 4;

	private boolean princessStage, gunStage, dynamiteStage, rlStage, nucStage, rlRlStage, dynamiteRlStage, dynamiteGunStage, gunGunStage, endStage;
	float delta, deltaAnimation;
	private float velocity = 20;
	private float princessComeTime = 0;
	private float shootTimer = 1f;
	private float shootTimerDyGun = 0;
	private ArrayList<Bullet> bullets = new ArrayList<Bullet>();
	private ArrayList<Dynamite> dynamites = new ArrayList<Dynamite>();
	private ArrayList<Rocket> rockets = new ArrayList<Rocket>();
	private ArrayList<Nuclear> nuclears = new ArrayList<Nuclear>();

	private ArrayList<Rocket> rocketsRl = new ArrayList<Rocket>();
	private ArrayList<RocketR> rocketsRlRl = new ArrayList<RocketR>();
	private ArrayList<Dynamite> dynamitesDyRl = new ArrayList<Dynamite>();
	private ArrayList<Rocket> rocketsDyRl = new ArrayList<Rocket>();
	private ArrayList<Dynamite> dynamitesDyGun = new ArrayList<Dynamite>();
	private ArrayList<Bullet> bulletsDyGun = new ArrayList<Bullet>();
	private ArrayList<Bullet> bulletsGun = new ArrayList<Bullet>();
	private ArrayList<Bullet> bulletsGunGun = new ArrayList<Bullet>();

	private float stageTimer = 0;
	private boolean gunStageOver, dynamiteStageOver, rlStageOver, nucStageOver, rlRlStageOver, dynamiteRlStageOver, dynamiteGunStageOver, gunGunStageOver, endStageOver;

	public Stages(Princess princess, Shark shark, ArrayList<Child> childs, GameScreen game, World world) {
		this.princess = princess;
		this.shark = shark;
		this.childs = childs;
		this.game = game;
		this.world = world;
	}

	public void update(float delta, float deltaAnimation) {
		this.delta = delta;
		this.deltaAnimation = deltaAnimation;

		if (!gunStage && !rlStage && !dynamiteStage && !nucStage && !rlRlStage && !dynamiteGunStage && !dynamiteRlStage && !gunGunStage && !endStage)
			angryStage();

		// Dynamite
		if (shark.getKills() >= stageDynamite && !princessStage && !dynamiteStageOver) {
			for (Child child : childs) {
				child.setRunaway();
			}

			princess.resetPosition();
			princess.come();
			princessStage = true;
			game.setPrincessStage(true);
			princess.setSpeaking(true);
			princess.setBalloon(Resources.balloonDynamite);
		}

		if (princessComeTime > speakingTime && !dynamiteStageOver && shark.getKills() >= stageDynamite) {
			dynamiteStage = true;
			princessComeTime = 0;
			princess.setSpeaking(false);
		}

		if (dynamiteStage || dynamiteStageOver)
			for (Dynamite dynamite : dynamites) {
				dynamite.update();
			}

		for (Iterator<Dynamite> it = dynamites.iterator(); it.hasNext();) {
			Dynamite dynamite = it.next();
			if (dynamite.isWeg()) {
				dynamite.destroyBody();
				it.remove();
			}
		}

		if (dynamiteStage && !dynamiteStageOver)
			dynamiteStage();

		// Gun
		if (shark.getKills() >= stageGun && !princessStage && !gunStageOver) {
			for (Child child : childs) {
				child.setRunaway();
			}
			princess.come();
			princessStage = true;
			princess.setSpeaking(true);
			princess.setBalloon(Resources.balloonGun);
			game.setPrincessStage(true);
		}

		if (princessComeTime > speakingTime && !gunStageOver && shark.getKills() >= stageGun) {
			gunStage = true;
			princessComeTime = 0;
			princess.setSpeaking(false);
			Resources.mgSound.loop();
		}

		if (gunStage && !gunStageOver)
			gunStage();

		// Rocket
		if (shark.getKills() >= stageRl && !princessStage && !rlStageOver) {
			for (Child child : childs) {
				child.setRunaway();
			}
			princess.resetPosition();
			princess.come();
			princessStage = true;
			princess.setSpeaking(true);
			princess.setBalloon(Resources.balloonRl);
			game.setPrincessStage(true);
		}

		if (rlStage || rlStageOver)
			for (Rocket rocket : rockets) {
				rocket.update();
			}

		for (Iterator<Rocket> it = rockets.iterator(); it.hasNext();) {
			Rocket rocket = it.next();
			if (rocket.isWeg()) {
				rocket.destroyBody();
				it.remove();
			}
		}

		if (princessComeTime > speakingTime && !rlStageOver && shark.getKills() >= stageRl) {
			rlStage = true;
			princess.setSpeaking(false);
			princessComeTime = 0;
		}

		if (rlStage && !rlStageOver)
			rlStage();

		// Rocket Rocket
		if (shark.getKills() >= stageRlRl && !princessStage && !rlRlStageOver) {
			for (Child child : childs) {
				child.setRunaway();
			}
			princess.resetPosition();
			princess.come();
			princessStage = true;
			princess.setSpeaking(true);
			princess.setBalloon(Resources.balloonRlRl);
			game.setPrincessStage(true);
		}

		if (rlRlStage || rlRlStageOver)
			for (Rocket rocket : rocketsRl) {
				rocket.update();
			}

		if (rlRlStage || rlRlStageOver)
			for (RocketR rocket : rocketsRlRl) {
				rocket.update();
			}

		for (Iterator<Rocket> it = rocketsRl.iterator(); it.hasNext();) {
			Rocket rocket = it.next();
			if (rocket.isWeg()) {
				rocket.destroyBody();
				it.remove();
			}
		}

		for (Iterator<RocketR> it = rocketsRlRl.iterator(); it.hasNext();) {
			RocketR rocket = it.next();
			if (rocket.isWeg()) {
				rocket.destroyBody();
				it.remove();
			}
		}

		if (princessComeTime > speakingTime && !rlRlStageOver && shark.getKills() >= stageRlRl) {
			rlRlStage = true;
			princess.setSpeaking(false);
			princessComeTime = 0;
		}

		if (rlRlStage && !rlRlStageOver)
			rlRlStage();

		// Nuclear
		if (shark.getKills() >= stageNuc && !princessStage && !nucStageOver) {
			for (Child child : childs) {
				child.setRunaway();
			}
			princess.resetPosition();
			princess.come();
			princessStage = true;
			princess.setSpeaking(true);
			princess.setBalloon(Resources.balloonNuke);
			game.setPrincessStage(true);
		}

		if (princessComeTime > speakingTime && !nucStageOver && shark.getKills() >= stageNuc) {
			nucStage = true;
			princess.setSpeaking(false);
			princessComeTime = 0;
		}

		if (nucStage || nucStageOver)
			for (Nuclear nuclear : nuclears) {
				nuclear.update();
			}

		for (Iterator<Nuclear> it = nuclears.iterator(); it.hasNext();) {
			Nuclear nuclear = it.next();
			if (nuclear.isWeg()) {
				nuclear.destroyBody();
				it.remove();
			}
		}

		if (nucStage && !nucStageOver)
			nucStage();

		// Rocket Dynamite
		if (shark.getKills() >= stageDynamiteRl && !princessStage && !dynamiteRlStageOver) {
			for (Child child : childs) {
				child.setRunaway();
			}
			princess.resetPosition();
			princess.come();
			princessStage = true;
			princess.setSpeaking(true);
			princess.setBalloon(Resources.balloonDynamiteRl);
			game.setPrincessStage(true);
		}

		if (dynamiteRlStage || dynamiteRlStageOver)
			for (Rocket rocket : rocketsDyRl) {
				rocket.update();
			}

		for (Iterator<Rocket> it = rocketsDyRl.iterator(); it.hasNext();) {
			Rocket rocket = it.next();
			if (rocket.isWeg()) {
				rocket.destroyBody();
				it.remove();
			}
		}

		if (dynamiteRlStage || dynamiteRlStageOver)
			for (Dynamite dynamite : dynamitesDyRl) {
				dynamite.update();
			}

		for (Iterator<Dynamite> it = dynamitesDyRl.iterator(); it.hasNext();) {
			Dynamite dynamite = it.next();
			if (dynamite.isWeg()) {
				dynamite.destroyBody();
				it.remove();
			}
		}

		if (princessComeTime > speakingTime && !dynamiteRlStageOver && shark.getKills() >= stageDynamiteRl) {
			dynamiteRlStage = true;
			princess.setSpeaking(false);
			princessComeTime = 0;
		}

		if (dynamiteRlStage && !dynamiteRlStageOver)
			dynamiteRlStage();

		// Dynamite Gun
		if (shark.getKills() >= stageDynamiteGun && !princessStage && !dynamiteGunStageOver) {
			for (Child child : childs) {
				child.setRunaway();
			}
			princess.resetPosition();
			princess.come();
			princessStage = true;
			princess.setSpeaking(true);
			princess.setBalloon(Resources.balloonDynamiteGun);
			game.setPrincessStage(true);
		}

		if (dynamiteGunStage || dynamiteGunStageOver)
			for (Dynamite dynamite : dynamitesDyGun) {
				dynamite.update();
			}

		for (Iterator<Dynamite> it = dynamitesDyGun.iterator(); it.hasNext();) {
			Dynamite dynamite = it.next();
			if (dynamite.isWeg()) {
				dynamite.destroyBody();
				it.remove();
			}
		}

		if (princessComeTime > speakingTime && !dynamiteGunStageOver && shark.getKills() >= stageDynamiteGun) {
			dynamiteGunStage = true;
			princess.setSpeaking(false);
			princessComeTime = 0;
		}

		if (dynamiteGunStage && !dynamiteGunStageOver)
			dynamiteGunStage();

		// GunGun
		if (shark.getKills() >= stageGunGun && !princessStage && !gunGunStageOver) {
			for (Child child : childs) {
				child.setRunaway();
			}
			princess.resetPosition();
			princess.come();
			princessStage = true;
			princess.setSpeaking(true);
			princess.setBalloon(Resources.balloonGunGun);
			game.setPrincessStage(true);
		}

		if (princessComeTime > speakingTime && !gunGunStageOver && shark.getKills() >= stageGunGun) {
			gunGunStage = true;
			princessComeTime = 0;
			princess.setSpeaking(false);
			Resources.mgSound.loop();
		}

		if (gunGunStage && !gunGunStageOver)
			gunGunStage();

		// End
		if (shark.getKills() >= stageEnd && !princessStage && !endStageOver) {
			for (Child child : childs) {
				child.setRunaway();
			}
			princess.resetPosition();
			princess.come();
			princessStage = true;
			princess.setSpeaking(true);
			princess.setBalloon(Resources.balloonEnd);
			game.setPrincessStage(true);
		}

		if (princessComeTime > speakingTime && !endStageOver && shark.getKills() >= stageEnd) {
			princess.getBody().setUserData("princessEnd");
			endStage = true;
			princessComeTime = 0;
			princess.setSpeaking(false);
		}

		if (endStage && !endStageOver)
			endStage();

	}

	public void angryStage() {
		princess.update();
		if (shark.getKills() >= stageDynamite && !dynamiteStageOver)
			princessComeTime += delta;
		if (shark.getKills() >= stageGun && !gunStageOver)
			princessComeTime += delta;
		if (shark.getKills() >= stageRl && !rlStageOver)
			princessComeTime += delta;
		if (shark.getKills() >= stageRlRl && !rlRlStageOver)
			princessComeTime += delta;
		if (shark.getKills() >= stageNuc && !nucStageOver)
			princessComeTime += delta;
		if (shark.getKills() >= stageDynamiteRl && !dynamiteRlStageOver)
			princessComeTime += delta;
		if (shark.getKills() >= stageDynamiteGun && !dynamiteGunStageOver)
			princessComeTime += delta;
		if (shark.getKills() >= stageGunGun && !gunGunStageOver)
			princessComeTime += delta;
		if (shark.getKills() >= stageEnd && !endStageOver)
			princessComeTime += delta;
		princess.setTexture(Resources.princessAngry.getKeyFrame(deltaAnimation, true));
	}

	public void gunStage() {
		stageTimer += delta;
		shootTimer += delta;
		if (shootTimer > 0.05f) {
			bullets.add(new Bullet(world, princess, shark));
			shootTimer = 0;
		}
		princess.setTexture(Resources.princessGun.getKeyFrame(deltaAnimation, true));

		if (princess.getBody().getPosition().x >= 139) {
			princess.getBody().setLinearVelocity(-velocity, 0);
		}
		if (princess.getBody().getPosition().x < 120) {
			princess.getBody().setLinearVelocity(velocity, 0);
		}

		for (Iterator<Bullet> it = bullets.iterator(); it.hasNext();) {
			Bullet bullet = it.next();
			if (bullet.getX() < 0 || bullet.getY() > 100) {
				bullet.destroyBody();
				it.remove();
			}
		}

		if (stageTimer > gunStageTime) {
			for (Iterator<Bullet> it = bullets.iterator(); it.hasNext();) {
				Bullet bullet = it.next();
				bullet.destroyBody();
				it.remove();
			}

			stageTimer = 0;
			shootTimer = 0.2f;
			princess.go();
			gunStage = false;
			gunStageOver = true;
			princessStage = false;
			Resources.mgSound.stop();
			game.setPrincessStage(false);
		}
	}

	public void dynamiteStage() {
		stageTimer += delta;
		shootTimer += delta;

		if (shootTimer >= 0.6f) {
			dynamites.add(new Dynamite(world, princess, shark));
			shootTimer = 0;
		}

		princess.setTexture(Resources.princessDynamite.getKeyFrame(deltaAnimation, true));

		if (princess.getBody().getPosition().x >= 139) {
			princess.getBody().setLinearVelocity(-velocity, 0);
		}
		if (princess.getBody().getPosition().x < 120) {
			princess.getBody().setLinearVelocity(velocity, 0);
		}

		if (stageTimer > dynamiteStageTime) {
			stageTimer = 0;
			shootTimer = 0;
			princess.go();
			dynamiteStage = false;
			dynamiteStageOver = true;
			princessStage = false;
			game.setPrincessStage(false);
		}
	}

	public void rlStage() {

		stageTimer += delta;
		shootTimer += delta;

		if (shootTimer >= 0.5f) {
			rockets.add(new Rocket(world, princess, shark));
			shootTimer = 0;
		}

		princess.setTexture(Resources.princessRL.getKeyFrame(deltaAnimation, true));

		if (princess.getBody().getPosition().x >= 139) {
			princess.getBody().setLinearVelocity(-velocity, 0);
		}
		if (princess.getBody().getPosition().x < 120) {
			princess.getBody().setLinearVelocity(velocity, 0);
		}

		if (stageTimer > rlStageTime) {
			stageTimer = 0;
			shootTimer = 0;
			princess.go();
			rlStage = false;
			rlStageOver = true;
			princessStage = false;
			game.setPrincessStage(false);
		}

	}

	public void rlRlStage() {

		stageTimer += delta;
		shootTimer += delta;

		if (shootTimer >= 0.8f) {
			rocketsRl.add(new Rocket(world, princess, shark));
			rocketsRlRl.add(new RocketR(world, princess, shark));
			shootTimer = 0;
		}

		princess.setTexture(Resources.princessRlRl.getKeyFrame(deltaAnimation, true));

		if (princess.getBody().getPosition().x >= 139) {
			princess.getBody().setLinearVelocity(-velocity, 0);
		}
		if (princess.getBody().getPosition().x < 120) {
			princess.getBody().setLinearVelocity(velocity, 0);
		}

		if (stageTimer > rlRlStageTime) {
			stageTimer = 0;
			shootTimer = 0;
			princess.go();
			rlRlStage = false;
			rlRlStageOver = true;
			princessStage = false;
			game.setPrincessStage(false);
		}

	}

	public void nucStage() {

		stageTimer += delta;
		shootTimer += delta;

		if (shootTimer >= 1f) {
			nuclears.add(new Nuclear(world, princess, shark));
			shootTimer = 0;
		}

		princess.setTexture(Resources.princessNuc.getKeyFrame(deltaAnimation, true));

		if (princess.getBody().getPosition().x >= 139) {
			princess.getBody().setLinearVelocity(-velocity, 0);
		}
		if (princess.getBody().getPosition().x < 120) {
			princess.getBody().setLinearVelocity(velocity, 0);
		}

		if (stageTimer > nucStageTime) {
			stageTimer = 0;
			shootTimer = 0;
			princess.go();
			nucStage = false;
			nucStageOver = true;
			princessStage = false;
			game.setPrincessStage(false);
		}

	}

	public void dynamiteRlStage() {

		stageTimer += delta;
		shootTimer += delta;

		if (shootTimer >= 0.5f) {
			dynamitesDyRl.add(new Dynamite(world, princess, shark, true));
			rocketsDyRl.add(new Rocket(world, princess, shark));
			shootTimer = 0;
		}

		princess.setTexture(Resources.princessDynamiteRl.getKeyFrame(deltaAnimation, true));

		if (princess.getBody().getPosition().x >= 139) {
			princess.getBody().setLinearVelocity(-velocity, 0);
		}
		if (princess.getBody().getPosition().x < 120) {
			princess.getBody().setLinearVelocity(velocity, 0);
		}

		if (stageTimer > DynamiteRlStageTime) {
			stageTimer = 0;
			shootTimer = 0;
			princess.go();
			dynamiteRlStage = false;
			dynamiteRlStageOver = true;
			princessStage = false;
			game.setPrincessStage(false);
		}

	}

	public void dynamiteGunStage() {
		stageTimer += delta;
		shootTimer += delta;
		shootTimerDyGun += delta;
		if (shootTimer > 0.05f) {
			bulletsDyGun.add(new Bullet(world, princess, shark));
			shootTimer = 0;
		}

		if (shootTimerDyGun > 0.5) {
			dynamitesDyGun.add(new Dynamite(world, princess, shark, true));
			shootTimerDyGun = 0;
		}
		princess.setTexture(Resources.princessDynamiteGun.getKeyFrame(deltaAnimation, true));

		if (princess.getBody().getPosition().x >= 139) {
			princess.getBody().setLinearVelocity(-velocity, 0);
		}
		if (princess.getBody().getPosition().x < 120) {
			princess.getBody().setLinearVelocity(velocity, 0);
		}

		for (Iterator<Bullet> it = bulletsDyGun.iterator(); it.hasNext();) {
			Bullet bullet = it.next();
			if (bullet.getX() < 0 || bullet.getY() > 100) {
				bullet.destroyBody();
				it.remove();
			}
		}

		if (stageTimer > DynamiteGunStageTime) {
			for (Iterator<Bullet> it = bulletsDyGun.iterator(); it.hasNext();) {
				Bullet bullet = it.next();
				bullet.destroyBody();
				it.remove();
			}

			stageTimer = 0;
			shootTimer = 0.2f;
			princess.go();
			dynamiteGunStage = false;
			dynamiteGunStageOver = true;
			princessStage = false;
			Resources.mgSound.stop();
			game.setPrincessStage(false);
		}
	}

	public void gunGunStage() {
		stageTimer += delta;
		shootTimer += delta;
		if (shootTimer > 0.08f) {
			bulletsGun.add(new Bullet(world, princess, shark));
			bulletsGunGun.add(new Bullet(world, princess, shark, true));
			shootTimer = 0;
		}
		princess.setTexture(Resources.princessGunGun.getKeyFrame(deltaAnimation, true));

		if (princess.getBody().getPosition().x >= 139) {
			princess.getBody().setLinearVelocity(-velocity, 0);
		}
		if (princess.getBody().getPosition().x < 120) {
			princess.getBody().setLinearVelocity(velocity, 0);
		}

		for (Iterator<Bullet> it = bulletsGun.iterator(); it.hasNext();) {
			Bullet bullet = it.next();
			if (bullet.getX() < 0 || bullet.getY() > 100) {
				bullet.destroyBody();
				it.remove();
			}
		}
		for (Iterator<Bullet> it = bulletsGunGun.iterator(); it.hasNext();) {
			Bullet bullet = it.next();
			if (bullet.getX() < 0 || bullet.getY() > 100) {
				bullet.destroyBody();
				it.remove();
			}
		}

		if (stageTimer > gunGunStageTime) {
			for (Iterator<Bullet> it = bulletsGun.iterator(); it.hasNext();) {
				Bullet bullet = it.next();
				bullet.destroyBody();
				it.remove();
			}
			for (Iterator<Bullet> it = bulletsGunGun.iterator(); it.hasNext();) {
				Bullet bullet = it.next();
				bullet.destroyBody();
				it.remove();
			}

			stageTimer = 0;
			shootTimer = 0.2f;
			princess.go();
			gunGunStage = false;
			gunGunStageOver = true;
			princessStage = false;
			Resources.mgSound.stop();
			game.setPrincessStage(false);
		}
	}

	public void endStage() {
		stageTimer += delta;

		System.out.println(princess.getBody().getUserData());
		
		if (!princess.isSwimming())
			princess.setTexture(new TextureRegion(Resources.princessEnd));
		else{
			princess.setTexture(new TextureRegion(Resources.princessEndSwimming));

		}

		

		if (princess.getBody().getPosition().x >= 75 && princess.getBody().getPosition().y <= 50)
			princess.getBody().setLinearVelocity(-7.5f * 1.5f, 5f * 1.5f);
		else
			princess.getBody().setLinearVelocity(-0.000001f, 0.000001f);
		
		if(princess.timeDead(delta) > 10f){
			game.getMain().setScreen(new EndScreen(game.getMain()));
		}

		if (stageTimer > endStageTime) {
			stageTimer = 0;
			shootTimer = 0;
			princess.go();
			endStage = false;
			endStageOver = true;
			princessStage = false;
			game.setPrincessStage(false);
		}
	}

	public ArrayList<Bullet> getBullets() {
		return bullets;
	}

	public ArrayList<Dynamite> getDynamites() {
		return dynamites;
	}

	public ArrayList<Rocket> getRockets() {
		return rockets;
	}

	public ArrayList<Nuclear> getNuclears() {
		return nuclears;
	}

	public ArrayList<Rocket> getRocketsRl() {
		return rocketsRl;
	}

	public ArrayList<RocketR> getRocketsRlRl() {
		return rocketsRlRl;
	}

	public ArrayList<Dynamite> getDynamitesDyRl() {
		return dynamitesDyRl;
	}

	public ArrayList<Rocket> getRocketsDyRl() {
		return rocketsDyRl;
	}

	public ArrayList<Dynamite> getDynamitesDyGun() {
		return dynamitesDyGun;
	}

	public ArrayList<Bullet> getBulletsDyGun() {
		return bulletsDyGun;
	}

	public ArrayList<Bullet> getBulletsGun() {
		return bulletsGun;
	}

	public ArrayList<Bullet> getBulletsGunGun() {
		return bulletsGunGun;
	}

}
