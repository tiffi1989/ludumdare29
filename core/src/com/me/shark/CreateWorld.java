package com.me.shark;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class CreateWorld {
	
	private World world;

	
	public CreateWorld(World world) {
		this.world = world;
		createBody(new Vector2(0, 50), 1, 150);
		createBody(new Vector2(150, 50), 1, 150);
		createBody(new Vector2(75, 100), 150, 1);
		createBody(new Vector2(75, 0), 150, 1);
		
		createBeach();
	}
	
	private void createBeach(){
		Body body;
		BodyDef bodyDef;
		FixtureDef fixtureDef;
		PolygonShape polygonShape;
		Fixture fixture;
		
		bodyDef = new BodyDef();
		bodyDef.type = BodyType.StaticBody;
		bodyDef.position.set(150,0);
		body = world.createBody(bodyDef);
		body.setUserData("beach");
		
		polygonShape = new PolygonShape();
		Vector2[] vertices = new Vector2[3];
		vertices[0] = new Vector2(-70, 0);
		vertices[1] = new Vector2(30, 0);
		vertices[2] = new Vector2(0, 37);
		polygonShape.set(vertices);
		

		
		fixtureDef = new FixtureDef();
		fixtureDef.shape = polygonShape;
		fixture = body.createFixture(fixtureDef);
		fixture.setUserData("beach");
	}
	
	private void createBody(Vector2 position, float height, float width ){
		Body body;
		BodyDef bodyDef;
		FixtureDef fixtureDef;
		PolygonShape polygonShape;
		Fixture fixture;
		
		bodyDef = new BodyDef();
		bodyDef.type = BodyType.StaticBody;
		bodyDef.position.set(position);
		body = world.createBody(bodyDef);
		body.setUserData("border");
		
		polygonShape = new PolygonShape();
		polygonShape.setAsBox(height, width);

		
		fixtureDef = new FixtureDef();
		fixtureDef.shape = polygonShape;
		fixture = body.createFixture(fixtureDef);
		

	}
}
