package com.me.shark;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

public class Child {

	private Body body;
	private BodyDef bodyDef;
	private CircleShape circleShape;
	private FixtureDef fixtureDef;
	private Fixture fixture;
	private int velocity = 20;
	private float radius = 5f;
	private Texture currentTexture;
	private Vector2 start, end;
	private boolean toStart, runaway;
	private World world;
	private float timeDead = 0;

	public Child(World world) {

		this.world = world;

		bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		int swithInt = (int) (Math.random() * 4);
		switch (swithInt) {
		case 0:
			bodyDef.position.set(150, 0);
			break;
		case 1:
			bodyDef.position.set(0, 0);
			break;
		case 2:
			bodyDef.position.set(150, 100);
			break;
		case 3:
			bodyDef.position.set(0, 100);
			break;

		default:
			bodyDef.position.set(150, 0);
			break;
		}

		body = world.createBody(bodyDef);
		body.setUserData("child");

		circleShape = new CircleShape();
		circleShape.setRadius(radius);

		fixtureDef = new FixtureDef();
		fixtureDef.shape = circleShape;
		fixtureDef.density = 0.5f;
		fixtureDef.friction = 0.4f;
		fixtureDef.restitution = 0.6f;
		fixtureDef.isSensor = true;

		fixture = body.createFixture(fixtureDef);

		currentTexture = Resources.childL;

		createPlan();

		int rnd = (int) (Math.random() * 4);
		switch (rnd) {
		case 0:
			currentTexture = Resources.childD;
			break;
		case 1:
			currentTexture = Resources.childL;
			break;
		case 2:
			currentTexture = Resources.childR;
			break;
		case 3:
			currentTexture = Resources.childU;
			break;

		default:
			currentTexture = Resources.childD;
			break;
		}

	}

	public void createPlan() {
		start = new Vector2((float) Math.random() * 140 + 5, (float) Math.random() * 90 + 5);
		end = new Vector2((float) Math.random() * 140 + 5, (float) Math.random() * 90 + 5);

		moveToEndPoint();
	}

	public void move() {
		if (!runaway) {
			if (toStart) {
				if (start.x > end.x) {
					if (body.getPosition().x > start.x) {
						moveToEndPoint();
					}
				} else {
					if (body.getPosition().x < start.x) {
						moveToEndPoint();
					}
				}
			} else {
				if (end.x > start.x) {
					if (body.getPosition().x > end.x) {
						moveToStartPoint();
					}
				} else {
					if (body.getPosition().x < end.x) {
						moveToStartPoint();
					}
				}
			}
			if (toStart) {
				if (start.y > end.y) {
					if (body.getPosition().y > start.y) {
						moveToEndPoint();
					}
				} else {
					if (body.getPosition().y < start.y) {
						moveToEndPoint();
					}
				}
			} else {
				if (end.y > start.y) {
					if (body.getPosition().y > end.y) {
						moveToStartPoint();
					}
				} else {
					if (body.getPosition().y < end.y) {
						moveToStartPoint();
					}
				}
			}
		}

	}

	public void moveToEndPoint() {
		Vector2 tmp = new Vector2(end.x - body.getPosition().x, end.y - body.getPosition().y);
		body.setLinearVelocity(tmp.nor().scl(velocity));
		toStart = false;
	}

	public void moveToStartPoint() {
		Vector2 tmp = new Vector2(start.x - body.getPosition().x, start.y - body.getPosition().y);
		body.setLinearVelocity(tmp.nor().scl(velocity));
		toStart = true;
	}

	public Texture getTexture() {
		if (body.getUserData().equals("dead"))
			return Resources.childDead;
		else
			return currentTexture;
	}

	public float getX() {
		return body.getPosition().x - radius * 1.25f;
	}

	public float getY() {
		return body.getPosition().y - radius * 1.25f;
	}

	public float getWidth() {
		return radius * 2.5f;
	}

	public float getHeight() {
		return radius * 2.5f;
	}

	public Body getBody() {
		return body;
	}

	public void destroyBody() {
		world.destroyBody(body);
	}

	public float timeDead(float delta) {
		if (body.getUserData().equals("dead")) {
			body.setLinearVelocity(0, 0);
			timeDead += delta;
		}
		return timeDead;
	}

	public float getTimeDead() {
		return timeDead;
	}

	public Texture getBloodImage() {
		Texture tmp = Resources.blood;
		return tmp;
	}

	public float getBloodX() {
		return body.getPosition().x - radius * 1.25f * timeDead;
	}

	public float getBloodY() {
		return body.getPosition().y - radius * 1.25f * timeDead;
	}

	public void setRunaway() {
		if (!runaway && !body.getUserData().equals("dead")) {
			body.setUserData("runaway");
			body.setLinearVelocity(new Vector2((float) Math.random(), (float) Math.random() - 1f).scl(velocity * 2));
		}
		runaway = true;
	}

}
