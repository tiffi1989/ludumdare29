package com.me.shark;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;

public class GameScreen implements Screen {
	private Main main;
	private float deltaAnimation = 0;
	private World world;
	private Box2DDebugRenderer debugRenderer;
	private OrthographicCamera camera;
	private int width, height;
	private Shark shark;
	private Princess princess;
	private ArrayList<Child> childs = new ArrayList<Child>();
	private CreateWorld createWorld;
	private float kiddieTimer = 0;
	private MyCollisionListener collisionListener;
	private BitmapFont font = new BitmapFont();
	private BitmapFont fontSpeach = new BitmapFont(Gdx.files.internal("font/visitor.fnt"), Gdx.files.internal("font/visitor.png"), false);
	private boolean princessStage;
	private Stages stages;

	public GameScreen(Main main) {
		width = Gdx.graphics.getWidth();
		height = Gdx.graphics.getHeight();
		this.main = main;
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 150, 100);
		camera.position.x = 150 / 2;
		camera.position.y = 100 / 2;

		font.setScale(0.5f);
		fontSpeach.setScale(0.2f);
		fontSpeach.setColor(Color.BLACK);

		collisionListener = new MyCollisionListener(this);
		world = new World(new Vector2(0, 0), true);
		debugRenderer = new Box2DDebugRenderer();
		world.setContactListener(collisionListener);

		shark = new Shark(world);
		princess = new Princess(world, shark);

		stages = new Stages(princess, shark, childs, this, world);

		createWorld = new CreateWorld(world);

	}

	public void update(float delta) {
		world.step(1 / 60f, 6, 2);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		update(delta);

		deltaAnimation += delta;
		kiddieTimer += delta;

		if (kiddieTimer > 0.5f && !princessStage) {
			childs.add(new Child(world));
			kiddieTimer = 0;
		}

		camera.update();

		shark.control(main);

		stages.update(delta, deltaAnimation);

		for (Child child : childs) {
			child.move();
		}

		for (Iterator<Child> it = childs.iterator(); it.hasNext();) {
			Child child = it.next();
			if (child.getBody().getUserData().equals("dead") || child.getBody().getUserData().equals("runaway")) {
				if (child.timeDead(delta) > 10 || child.getBody().getPosition().x > 160 || child.getBody().getPosition().x < -10 || child.getBody().getPosition().y > 110 || child.getBody().getPosition().y < -10) {
					child.destroyBody();
					it.remove();
				}
			}
		}

		main.batch.setProjectionMatrix(camera.combined);

		main.batch.begin();
		

		main.batch.draw(Resources.backgroundStatic, 0, 0, camera.viewportWidth, camera.viewportHeight);

		for (Child child : childs) {
			if (child.getBody().getUserData().equals("dead")) {
				main.batch.setColor(1, 1, 1, 1 / (1 + child.getTimeDead() / 2));
				main.batch.draw(child.getBloodImage(), child.getBloodX(), child.getBloodY(), child.getWidth() * child.getTimeDead(), child.getHeight() * child.getTimeDead());
			}
		}
		if (shark.isDead()) {
			shark.timeDead(delta);
			main.batch.setColor(1, 1, 1, 1 / (1 + shark.getTimeDead() / 6));
			main.batch.draw(Resources.blood, shark.getBloodX(), shark.getBloodY(), shark.getWidth() * shark.getTimeDead() / 2, shark.getHeight() * shark.getTimeDead());
		}
		if (princess.isDead()) {
			princess.timeDead(delta);
			main.batch.setColor(1, 1, 1, 1 / (1 + princess.getTimeDead() / 6));
			main.batch.draw(Resources.blood, princess.getBloodX(), princess.getBloodY(), princess.getWidth() * princess.getTimeDead(), princess.getHeight() * princess.getTimeDead());
			float x = princess.getX() - princess.getWidth()*2;
			float y = princess.getY() + princess.getHeight();
			main.batch.setColor(1, 1, 1, 1);
			main.batch.draw(Resources.balloonDead, x, y, princess.getWidth() *2, princess.getHeight() *2);
		}

		main.batch.setColor(1, 1, 1, 1);
		main.batch.draw(Resources.background.getKeyFrame(deltaAnimation, true), 0, 0, camera.viewportWidth, camera.viewportHeight);
		

		for (Child child : childs) {
			main.batch.draw(child.getTexture(), child.getX(), child.getY(), child.getWidth(), child.getHeight());
		}
		if (shark.isMoving())
			main.batch.draw(shark.getDirection(deltaAnimation), shark.getX(), shark.getWaterY(), shark.getWidth(), shark.getHeight());
		main.batch.draw(shark.getTexture(), shark.getX(), shark.getY(), shark.getWidth(), shark.getHeight());
		
		if(princess.isSpeaking()){
			float x = princess.getX() - princess.getWidth()*2;
			float y = princess.getY() + princess.getHeight();
			main.batch.draw(princess.getBalloon(), x, y, princess.getWidth() *2, princess.getHeight() *2);
		}
		
		for (Bullet bullet : stages.getBullets()) {
			main.batch.draw(bullet.getTexture(), bullet.getX(), bullet.getY(), bullet.getWidth(), bullet.getHeight());
		}
		for(Dynamite dynamite : stages.getDynamites()){
			main.batch.draw(dynamite.getTexture(delta), dynamite.getX(), dynamite.getY(), dynamite.getWidth(), dynamite.getHeight());
		}
		for(Rocket rocket : stages.getRockets()){
			main.batch.draw(rocket.getTexture(delta), rocket.getX(), rocket.getY(), rocket.getWidth()/2, rocket.getHeight()/2, rocket.getWidth() , rocket.getHeight() ,1f,1f , rocket.getVector().angle() - 90);
		}
		for(Nuclear nuclear : stages.getNuclears()){
			main.batch.draw(nuclear.getTexture(delta), nuclear.getX(), nuclear.getY(),  nuclear.getWidth() , nuclear.getHeight());
		}
		for(Rocket rocket : stages.getRocketsRl()){
			main.batch.draw(rocket.getTexture(delta), rocket.getX(), rocket.getY(), rocket.getWidth()/2, rocket.getHeight()/2, rocket.getWidth() , rocket.getHeight() ,1f,1f , rocket.getVector().angle() - 90);
		}
		for(RocketR rocket : stages.getRocketsRlRl()){
			main.batch.draw(rocket.getTexture(delta), rocket.getX(), rocket.getY(), rocket.getWidth()/2, rocket.getHeight()/2, rocket.getWidth() , rocket.getHeight() ,1f,1f , rocket.getVector().angle() - 90);
		}
		for(Rocket rocket : stages.getRocketsDyRl()){
			main.batch.draw(rocket.getTexture(delta), rocket.getX(), rocket.getY(), rocket.getWidth()/2, rocket.getHeight()/2, rocket.getWidth() , rocket.getHeight() ,1f,1f , rocket.getVector().angle() - 90);
		}
		for(Dynamite dynamite : stages.getDynamitesDyRl()){
			main.batch.draw(dynamite.getTexture(delta), dynamite.getX(), dynamite.getY(), dynamite.getWidth(), dynamite.getHeight());
		}
		for(Dynamite dynamite : stages.getDynamitesDyGun()){
			main.batch.draw(dynamite.getTexture(delta), dynamite.getX(), dynamite.getY(), dynamite.getWidth(), dynamite.getHeight());
		}
		for (Bullet bullet : stages.getBulletsDyGun()) {
			main.batch.draw(bullet.getTexture(), bullet.getX(), bullet.getY(), bullet.getWidth(), bullet.getHeight());
		}
		for (Bullet bullet : stages.getBulletsGunGun()) {
			main.batch.draw(bullet.getTexture(), bullet.getX(), bullet.getY(), bullet.getWidth(), bullet.getHeight());
		}
		for (Bullet bullet : stages.getBulletsGun()) {
			main.batch.draw(bullet.getTexture(), bullet.getX(), bullet.getY(), bullet.getWidth(), bullet.getHeight());
		}

		font.draw(main.batch, "Kills: " + shark.getKills(), 5, 9);
		int tmp = 5;

		while(tmp <= shark.getHealth()){
			main.batch.draw(Resources.heart, -3 + tmp, 94, 4, 4);
			tmp += 5;
		}
//		font.draw(main.batch, "Health: " + shark.getHealth(), 5, 90);
		
		main.batch.draw(princess.getTexture(deltaAnimation), princess.getX(), princess.getY(), princess.getWidth(), princess.getHeight());


		main.batch.end();

//		debugRenderer.render(world, camera.combined);

	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

	public Shark getShark() {
		return shark;
	}

	public void setPrincessStage(boolean bool) {
		princessStage = bool;
	}

	public boolean getPrincessStage() {
		return princessStage;
	}
	
	public Princess getPrincess(){
		return princess;
	}
	
	public Main getMain(){
		return main;
	}

}
