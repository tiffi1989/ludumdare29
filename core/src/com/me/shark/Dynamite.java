package com.me.shark;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

public class Dynamite {

	private Body body;
	private BodyDef bodyDef;
	private CircleShape circleShape;
	private FixtureDef fixtureDef;
	private Fixture fixture;
	private int velocity = 50;
	private float radius = 3f;
	private World world;
	private float time = 0;
	float x, y, countdown, delta;
	private TextureRegion currentTexture;
	private boolean weg, secondBody;
	
	public Dynamite(World world, Princess princess, Shark shark, boolean right) {
		this.world = world;

		countdown = 0.8f;

		bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(princess.getBody().getPosition().x + princess.getBody().getFixtureList().get(0).getShape().getRadius(), princess.getBody().getPosition().y);
		body = world.createBody(bodyDef);
		body.setUserData("dyn");

		circleShape = new CircleShape();
		circleShape.setRadius(radius);

		fixtureDef = new FixtureDef();
		fixtureDef.shape = circleShape;
		fixtureDef.isSensor = true;

		fixture = body.createFixture(fixtureDef);
		x = (float) Math.random() * 140 + 5;
		y = (float) Math.random() * 90 + 5;
		if(x > 80 && y < 40 ){
			x = shark.getBody().getPosition().x;
			y = shark.getBody().getPosition().y;
			System.out.println(x + " " + y);
		}
		float xVelo = x - princess.getBody().getPosition().x;
		float yVelo = y - princess.getBody().getPosition().y;
		body.setLinearVelocity(new Vector2(xVelo, yVelo).nor().scl(velocity));

		currentTexture = Resources.dynamite.getKeyFrame(0, false);
	}

	public Dynamite(World world, Princess princess, Shark shark) {
		this.world = world;

		countdown = 0.8f;

		bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(princess.getBody().getPosition().x - princess.getBody().getFixtureList().get(0).getShape().getRadius(), princess.getBody().getPosition().y);
		body = world.createBody(bodyDef);
		body.setUserData("dyn");

		circleShape = new CircleShape();
		circleShape.setRadius(radius);

		fixtureDef = new FixtureDef();
		fixtureDef.shape = circleShape;
		fixtureDef.isSensor = true;

		fixture = body.createFixture(fixtureDef);
		x = (float) Math.random() * 140 + 5;
		y = (float) Math.random() * 90 + 5;
		if(x > 80 && y < 40 ){
			x = shark.getBody().getPosition().x;
			y = shark.getBody().getPosition().y;
			System.out.println(x + " " + y);
		}
		float xVelo = x - princess.getBody().getPosition().x;
		float yVelo = y - princess.getBody().getPosition().y;
		body.setLinearVelocity(new Vector2(xVelo, yVelo).nor().scl(velocity));

		currentTexture = Resources.dynamite.getKeyFrame(0, false);
	}

	public float getX() {
		return body.getPosition().x - radius * 1f;
	}

	public float getY() {
		return body.getPosition().y - radius * 1f;
	}

	public float getWidth() {
		return radius * 2f;
	}

	public float getHeight() {
		return radius * 2f;
	}

	public TextureRegion getTexture(float delta) {
		this.delta = delta;
		return currentTexture;
	}

	public void destroyBody() {
		world.destroyBody(body);
	}

	public void update() {
		if ((body.getPosition().x < x && body.getPosition().y > y) && time < countdown) {
			body.setLinearVelocity(0, 0);
			time += delta;
			currentTexture = Resources.dynamite.getKeyFrame(time, false);
		}

		if (time >= countdown + 1f) {
			weg = true;
		}
		if (time >= countdown) {
			time += delta;
			radius = 10;
			currentTexture = Resources.explosion.getKeyFrame(time, true);
			createNewBody();
			body.setUserData("dynamite");
		}

	}

	public void createNewBody() {
		if (!secondBody) {
			Resources.explosionSound.play();
			body.destroyFixture(fixture);
			circleShape.setRadius(15);
			fixtureDef = new FixtureDef();
			fixtureDef.shape = circleShape;
			fixtureDef.isSensor = true;
			fixture = body.createFixture(fixtureDef);
			secondBody = true;
		}
	}

	public boolean isWeg() {
		return weg;
	}

}
