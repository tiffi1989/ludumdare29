package com.me.shark;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

public class Shark {
	private Body body, headBody;
	private BodyDef bodyDef, headBodyDef;
	private CircleShape circleShape;
	private FixtureDef fixtureDef, headFixtureDef;
	private Fixture fixture, headFixture;
	private int velocity = 50;
	private float radius = 3f;
	private Texture currentTexture;
	private int kills = 0;
	private int health = 100;
	private boolean dead;
	private float timeDead = 0;

	public Shark(World world) {
		bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(75, 50);
		body = world.createBody(bodyDef);
		body.setUserData("fin");

		circleShape = new CircleShape();
		circleShape.setRadius(radius);

		fixtureDef = new FixtureDef();
		fixtureDef.shape = circleShape;
		fixtureDef.density = 0.5f;
		fixtureDef.friction = 0.4f;
		fixtureDef.restitution = 0.6f;

		fixture = body.createFixture(fixtureDef);

		headBodyDef = new BodyDef();
		headBodyDef.type = BodyType.DynamicBody;
		headBodyDef.position.set(50, 50);
		headBody = world.createBody(headBodyDef);
		headBody.setUserData("shark");

		headFixtureDef = new FixtureDef();
		headFixtureDef.shape = circleShape;
		headFixtureDef.isSensor = true;

		headFixture = headBody.createFixture(headFixtureDef);

		currentTexture = Resources.finL;

	}

	public void control(Main main) {
		if (health > 0) {

			if (Gdx.input.isKeyPressed(Keys.UP)) {
				body.setLinearVelocity(body.getLinearVelocity().x, velocity);
				currentTexture = Resources.finU;
			} else if (Gdx.input.isKeyPressed(Keys.DOWN)) {
				body.setLinearVelocity(body.getLinearVelocity().x, -velocity);
				currentTexture = Resources.finD;
			} else {
				body.setLinearVelocity(body.getLinearVelocity().x, 0);
			}

			if (Gdx.input.isKeyPressed(Keys.LEFT)) {
				body.setLinearVelocity(-velocity, body.getLinearVelocity().y);
				currentTexture = Resources.finL;
			} else if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
				body.setLinearVelocity(velocity, body.getLinearVelocity().y);
				currentTexture = Resources.finR;
			} else {
				body.setLinearVelocity(0, body.getLinearVelocity().y);
			}

			if (currentTexture.equals(Resources.finU))
				headBody.setTransform(body.getPosition().x + 3, body.getPosition().y + 10, 0);
			if (currentTexture.equals(Resources.finD))
				headBody.setTransform(body.getPosition().x + 3, body.getPosition().y - 10, 0);
			if (currentTexture.equals(Resources.finL))
				headBody.setTransform(body.getPosition().x - 10, body.getPosition().y - 3, 0);
			if (currentTexture.equals(Resources.finR))
				headBody.setTransform(body.getPosition().x + 10, body.getPosition().y - 3, 0);

		} else {
			body.setLinearVelocity(0, 0);
			headBody.setLinearVelocity(0, 0);
			if (timeDead > 6f)
				main.setScreen(new TryAgainScreen(main));
		}

	}

	public Texture getTexture() {
		if (health > 0)
			return currentTexture;
		else
			return Resources.sharkDead;
	}

	public float getX() {
		if (health > 0)
			return body.getPosition().x - radius;
		else
			return body.getPosition().x - radius * 4;
	}

	public float getY() {
		if (health > 0)
			return body.getPosition().y - radius;
		else
			return body.getPosition().y - radius * 2;
	}

	public float getWidth() {
		if (health > 0)
			return radius * 2;
		else
			return radius * 8;
	}

	public float getHeight() {
		if (health > 0)
			return radius * 2;
		else
			return radius * 4;
	}

	public boolean isMoving() {
		if (body.getLinearVelocity().x != 0 || body.getLinearVelocity().y != 0) {
			return true;
		} else {
			return false;
		}
	}

	public TextureRegion getDirection(float deltaAnimation) {
		if (currentTexture.equals(Resources.finU))
			return Resources.sharkWaterUp.getKeyFrame(deltaAnimation, true);
		if (currentTexture.equals(Resources.finD))
			return Resources.sharkWaterDo.getKeyFrame(deltaAnimation, true);
		if (currentTexture.equals(Resources.finL))
			return Resources.sharkWaterLe.getKeyFrame(deltaAnimation, true);
		if (currentTexture.equals(Resources.finR))
			return Resources.sharkWaterRi.getKeyFrame(deltaAnimation, true);

		return null;
	}

	public float getWaterY() {
		if (currentTexture.equals(Resources.finU))
			return getY();
		if (currentTexture.equals(Resources.finD))
			return getY();
		if (currentTexture.equals(Resources.finL))
			return getY() - 2f;
		if (currentTexture.equals(Resources.finR))
			return getY() - 2f;

		return 0;
	}

	public void killsPp() {
		kills++;
	}

	public int getKills() {
		return kills;
	}

	public Body getBody() {
		return body;
	}

	public void hit(int damage) {
		health = health - damage;
		if (health <= 0) {
			health = 0;
		}
		if (health > 0) {
			Resources.hit.play();
		}
		if (health == 0 && !dead) {
			dead = true;
			Resources.die.play();
			body.setUserData("deadShark");
		}
	}

	public int getHealth() {
		return health;
	}

	public boolean isDead() {
		return dead;
	}

	public float getBloodX() {
		return body.getPosition().x - radius * 2 * timeDead;
	}

	public float getBloodY() {
		return body.getPosition().y - radius * 2 * timeDead;
	}

	public float timeDead(float delta) {
		if (dead)
			timeDead += delta;
		return timeDead;
	}

	public float getTimeDead() {
		return timeDead;
	}

	public void heal(int amount) {
		if (health > 0)
			health += amount;
		if (health > 100)
			health = 100;
	}

}
