package com.me.shark;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class TryAgainScreen implements Screen {

	private Main main;
	private OrthographicCamera camera;
	
	public TryAgainScreen(Main main) {
		this.main = main;
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 150, 100);
		camera.position.x = 150 / 2;
		camera.position.y = 100 / 2;
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		camera.update();
		main.batch.setProjectionMatrix(camera.combined);
		
		main.batch.begin();
		main.batch.draw(Resources.tryAgain, 0, 0, 150, 100);
		main.batch.end();
		
		if(Gdx.input.isTouched() || Gdx.input.isKeyPressed(Keys.SPACE)){
			main.setScreen(new GameScreen(main));
			if(Resources.ruhig.isPlaying())
				Resources.ruhig.stop();
			if(Resources.schnell.isPlaying())
				Resources.schnell.stop();
		}
	}

	@Override
	public void resize(int width, int height) {		
	}

	@Override
	public void show() {		
	}

	@Override
	public void hide() {		
	}

	@Override
	public void pause() {		
	}

	@Override
	public void resume() {		
	}

	@Override
	public void dispose() {		
	}

}
