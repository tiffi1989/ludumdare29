package com.me.shark;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

public class Bullet {
	private Body body;
	private BodyDef bodyDef;
	private CircleShape circleShape;
	private FixtureDef fixtureDef;
	private Fixture fixture;
	private int velocity = 200;
	private float radius = 0.5f;
	private World world;

	public Bullet(World world, Princess princess, Shark shark) {
		this.world = world;

		bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(princess.getBody().getPosition().x - princess.getBody().getFixtureList().get(0).getShape().getRadius(), princess.getBody().getPosition().y);
		body = world.createBody(bodyDef);
		body.setUserData("bullet");

		circleShape = new CircleShape();
		circleShape.setRadius(radius);

		fixtureDef = new FixtureDef();
		fixtureDef.shape = circleShape;
		fixtureDef.density = 0.5f;
		fixtureDef.friction = 0.4f;
		fixtureDef.restitution = 0.6f;
		fixtureDef.isSensor = true;

		fixture = body.createFixture(fixtureDef);
		float xVelo = shark.getBody().getPosition().x - princess.getBody().getPosition().x;
		float yVelo = shark.getBody().getPosition().y - princess.getBody().getPosition().y;
		body.setLinearVelocity(new Vector2(xVelo, yVelo).nor().scl(velocity).rotate((float)Math.random()*30-15));
	}
	
	public Bullet(World world, Princess princess, Shark shark, boolean b) {
		this.world = world;

		bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(princess.getBody().getPosition().x - princess.getBody().getFixtureList().get(0).getShape().getRadius()*0.8f, princess.getBody().getPosition().y + 1f);
		body = world.createBody(bodyDef);
		body.setUserData("bullet");

		circleShape = new CircleShape();
		circleShape.setRadius(radius);

		fixtureDef = new FixtureDef();
		fixtureDef.shape = circleShape;
		fixtureDef.density = 0.5f;
		fixtureDef.friction = 0.4f;
		fixtureDef.restitution = 0.6f;
		fixtureDef.isSensor = true;

		fixture = body.createFixture(fixtureDef);
		float xVelo = shark.getBody().getPosition().x - princess.getBody().getPosition().x;
		float yVelo = shark.getBody().getPosition().y - princess.getBody().getPosition().y;
		body.setLinearVelocity(new Vector2(xVelo, yVelo).nor().scl(velocity).rotate((float)Math.random()*30-15));
	}

	public float getX() {
		return body.getPosition().x - radius * 1f;
	}

	public float getY() {
		return body.getPosition().y - radius * 1f;
	}

	public float getWidth() {
		return radius * 2f;
	}

	public float getHeight() {
		return radius * 2f;
	}
	
	public Texture getTexture(){
		return Resources.bullet;
	}
	
	public void destroyBody(){
		world.destroyBody(body);
	}

}
