package com.me.shark;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

public class MyCollisionListener implements ContactListener {

	private GameScreen game;

	public MyCollisionListener(GameScreen game) {
		this.game = game;
	}

	@Override
	public void beginContact(Contact contact) {
		if (contact.getFixtureA().getBody().getUserData().equals("shark") && contact.getFixtureB().getBody().getUserData().equals("child")) {
			contact.getFixtureB().getBody().setUserData("dead");
			game.getShark().killsPp();
			game.getShark().heal(10);
			Sound tmp = Resources.killSound2;
			tmp.play();

		}
		if (contact.getFixtureB().getBody().getUserData().equals("shark") && contact.getFixtureA().getBody().getUserData().equals("child")) {
			contact.getFixtureA().getBody().setUserData("dead");
			game.getShark().killsPp();
			game.getShark().heal(10);
			Sound tmp = Resources.killSound2;
			tmp.play();
		}

		if (contact.getFixtureA().getBody().getUserData().equals("fin") && contact.getFixtureB().getBody().getUserData().equals("bullet")) {
			game.getShark().hit(5);
		}
		if (contact.getFixtureB().getBody().getUserData().equals("fin") && contact.getFixtureA().getBody().getUserData().equals("bullet")) {
			game.getShark().hit(5);
		}

		if (contact.getFixtureA().getBody().getUserData().equals("fin") && contact.getFixtureB().getBody().getUserData().equals("dynamite")) {
			game.getShark().hit(20);
		}
		if (contact.getFixtureB().getBody().getUserData().equals("fin") && contact.getFixtureA().getBody().getUserData().equals("dynamite")) {
			game.getShark().hit(20);
		}
		if (contact.getFixtureA().getBody().getUserData().equals("shark") && contact.getFixtureB().getBody().getUserData().equals("princessEnd")) {
			game.getPrincess().setDead(true);
			Resources.killSound2.play();
		}
		if (contact.getFixtureB().getBody().getUserData().equals("shark") && contact.getFixtureA().getBody().getUserData().equals("princessEnd")) {
			game.getPrincess().setDead(true);
			Resources.killSound2.play();
		}

	}

	@Override
	public void endContact(Contact contact) {
		if (contact.getFixtureA() != null && contact.getFixtureB() != null)
			if (contact.getFixtureA().getUserData() != null && contact.getFixtureB().getUserData() != null) {
				if (contact.getFixtureA().getBody().getUserData().equals("princessEnd") && contact.getFixtureB().getBody().getUserData().equals("beach"))
					game.getPrincess().setSwimming(true);
				if (contact.getFixtureB().getBody().getUserData().equals("princessEnd") && contact.getFixtureA().getBody().getUserData().equals("beach"))
					game.getPrincess().setSwimming(true);
			}
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub

	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub

	}

}
