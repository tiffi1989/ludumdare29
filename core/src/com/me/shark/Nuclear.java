package com.me.shark;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

public class Nuclear {
	
	private Body body;
	private BodyDef bodyDef;
	private CircleShape circleShape;
	private FixtureDef fixtureDef;
	private Fixture fixture;
	private int velocity = 20;
	private float radius = 3f;
	private World world;
	private float time = 0;
	float x, y, countdown, delta;
	private TextureRegion currentTexture;
	private boolean weg, secondBody;

	public Nuclear(World world, Princess princess, Shark shark) {
		this.world = world;

		countdown = 1f;

		bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(princess.getBody().getPosition().x - princess.getBody().getFixtureList().get(0).getShape().getRadius(), princess.getBody().getPosition().y);
		body = world.createBody(bodyDef);
		body.setUserData("dyn");

		circleShape = new CircleShape();
		circleShape.setRadius(radius);

		fixtureDef = new FixtureDef();
		fixtureDef.shape = circleShape;
		fixtureDef.isSensor = true;

		fixture = body.createFixture(fixtureDef);
		x = (float) Math.random() * 140 + 5;
		y = (float) Math.random() * 90 + 5;
		if(x > 80 && y < 40 ){
			x = shark.getBody().getPosition().x;
			y = shark.getBody().getPosition().y;
			System.out.println(x + " " + y);
		}
		body.setTransform(x, y, 0);

		currentTexture = new TextureRegion(Resources.nuclearPointer);
	}

	public float getX() {
		return body.getPosition().x - radius * 1f;
	}

	public float getY() {
		return body.getPosition().y - radius * 1f;
	}

	public float getWidth() {
		return radius * 2f;
	}

	public float getHeight() {
		return radius * 2f;
	}

	public TextureRegion getTexture(float delta) {
		this.delta = delta;
		return currentTexture;
	}

	public void destroyBody() {
		world.destroyBody(body);
	}

	public void update() {
		time += delta;
		if (time >= countdown + 1f) {
			weg = true;
		}
		if (time >= countdown) {
			radius = 30;
			currentTexture = Resources.nuclear.getKeyFrame(time, true);
			createNewBody();
			body.setUserData("dynamite");
		}

	}

	public void createNewBody() {
		if (!secondBody) {
			Resources.nuke.play();
			body.destroyFixture(fixture);
			circleShape.setRadius(30);
			fixtureDef = new FixtureDef();
			fixtureDef.shape = circleShape;
			fixtureDef.isSensor = true;
			fixture = body.createFixture(fixtureDef);
			secondBody = true;
		}
	}

	public boolean isWeg() {
		return weg;
	}


}
