package com.me.shark;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Main extends Game {
	public SpriteBatch batch;
	private Resources resources;

	@Override
	public void create() {
		batch = new SpriteBatch();
		resources = new Resources();
//		 TexturePacker.process("D:/Workspaces/LD29/android/assets/res/In/", "D:/Workspaces/LD29/android/assets/res/Out/", "victoryScreen");

		this.setScreen(new MainMenuScreen(this));
	}

	@Override
	public void render() {
		super.render();
	}

}
