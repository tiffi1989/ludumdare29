package com.me.shark.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.me.shark.Main;

public class DesktopLauncher {
	public static void main (String[] arg) {
		new LwjglApplication(new Main(), "Sharks", 1280, 800);		
	}
}
